import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout {
    id: page
    property alias cfg_screenID: screenID.value

    SpinBox {
        id: screenID
        Kirigami.FormData.label: i18n("Screen ID:")
        //Kirigami.FormData.tooltip: i18n("Get via 'kscreen-doctor -o' (first value after 'Output:')")
    }
}
