// main.qml
import QtQuick 2.0
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.plasmoid 2.0

Item {
    // https://github.com/KDE/plasma-workspace/blob/master/dataengines/executable/executable.h
	// https://github.com/KDE/plasma-workspace/blob/master/dataengines/executable/executable.cpp
	// https://github.com/KDE/plasma-framework/blob/master/src/declarativeimports/core/datasource.h
	// https://github.com/KDE/plasma-framework/blob/master/src/declarativeimports/core/datasource.cpp
	// https://github.com/KDE/plasma-framework/blob/master/src/plasma/scripting/dataenginescript.cpp
	PlasmaCore.DataSource {
		id: executable
		engine: "executable"
		connectedSources: []
		onNewData: {
			var exitCode = data["exit code"]
			var exitStatus = data["exit status"]
			var stdout = data["stdout"]
			var stderr = data["stderr"]
			exited(sourceName, exitCode, exitStatus, stdout, stderr)
			disconnectSource(sourceName) // cmd finished
		}
		function exec(cmd) {
			if (cmd) {
				connectSource(cmd)
			}
		}
		signal exited(string cmd, int exitCode, int exitStatus, string stdout, string stderr)
	}

    function rotate(direction) {
        executable.exec("kscreen-doctor output." + Plasmoid.configuration.screenID + ".rotation." + direction)
    }
    // Always display the compact view.
    // Never show the full popup view even if there is space for it.
    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation

    Plasmoid.fullRepresentation: GridLayout {
        id: grid
        columns: 2 
        Layout.minimumWidth: label.implicitWidth
        Layout.minimumHeight: label.implicitHeight
        Layout.preferredWidth: 640 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredHeight: 480 * PlasmaCore.Units.devicePixelRatio
    
        PlasmaComponents.ToolButton {
            id: "none"
            tooltip: "None"
            onClicked: rotate("none")
            text: "None"

        }
        PlasmaComponents.ToolButton {
            id: "left"
            tooltip: "Left"
            onClicked: rotate("left")
            text: "Left"
            
        }
        PlasmaComponents.ToolButton {
            id: "right"
            tooltip: "Right"
            onClicked: rotate("right")
            text: "Right"
            
        }
        PlasmaComponents.ToolButton {
            id: "inverted"
            tooltip: "Inverted"
            onClicked: rotate("inverted")
            text: "Inverted"

        }
    
        
    }
}
